/*
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.jcae.netbeans.expandall;

import java.awt.Component;
import java.awt.Container;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import javax.swing.FocusManager;
import javax.swing.JDialog;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeModel;
import org.openide.explorer.view.TreeView;
import org.openide.util.Exceptions;
import org.openide.util.HelpCtx;
import org.openide.util.NbBundle;
import org.openide.util.actions.CallableSystemAction;

public final class ExpandAll extends CallableSystemAction
{
	private Method expandChild;
	
	public void performAction()
	{
		Component c = FocusManager.getCurrentManager().getFocusOwner();		
		TreeView tv = findInAncestors(c, TreeView.class);
		if(tv != null)
			tv.expandAll();
		Component root = findInAncestors(c, "org.netbeans.modules.xml.schema.abe.GlobalElementsContainerPanel");
		if(root != null)
		{	
			//showHierarchy(root);
			ArrayList<Component> list = new ArrayList<Component>();
			findInChild(root, "org.netbeans.modules.xml.schema.abe.ElementPanel", list);
			for(Component ep:list)
				expandChild(ep);
		}
	}
	
	private void expandChild(Object object)
	{
		try
		{
			if (expandChild == null)
				expandChild = object.getClass().getMethod("expandChild");
			expandChild.invoke(object);
		}
		catch (IllegalAccessException ex)
		{
			Exceptions.printStackTrace(ex);
		}
		catch (IllegalArgumentException ex)
		{
			Exceptions.printStackTrace(ex);
		}
		catch (InvocationTargetException ex)
		{
			Exceptions.printStackTrace(ex);
		}
		catch (NoSuchMethodException ex)
		{
			Exceptions.printStackTrace(ex);
		}
		catch (SecurityException ex)
		{
			Exceptions.printStackTrace(ex);
		}	 
	}
	
	private void findInChild(Component c, String type, Collection<Component> result)
	{
		if(type.equals(c.getClass().getName()))
			result.add(c);
		if(c instanceof Container)
		{
			for(Component cp:((Container)c).getComponents())
				findInChild(cp,type, result);
		}
	}
	
	private Component findInAncestors(Component c, String type)
	{
		if(c == null)
			return null;		
		else if(type.equals(c.getClass().getName()))
			return c;
		else
			return findInAncestors(c.getParent(), type);
		
	}
	private <T extends Component> T findInAncestors(Component c, Class<T> type)
	{
		if(c == null)
			return null;			
		else if(type.isAssignableFrom(c.getClass()))
			return (T)c;
		else
			return findInAncestors(c.getParent(), type);
	}
	
	
	private static void showHierarchy(Component c)
	{
		JDialog d = new JDialog();
		JTree tree = new JTree();
		tree.setModel(new DefaultTreeModel(null){

			@Override
			public Object getRoot()
			{
				return FocusManager.getCurrentManager().getFocusOwner().getParent().getParent().getParent();
			}

			@Override
			public Object getChild(Object parent, int index)
			{
				if(parent instanceof Container)
					return ((Container)parent).getComponent(index);
				else
					return null;
			}

			@Override
			public int getChildCount(Object parent)
			{
				if(parent instanceof Container)
					return  ((Container)parent).getComponentCount();
				else
					return 0;
			}

			@Override
			public boolean isLeaf(Object node)
			{
				return getChildCount(node) == 0;
			}

			@Override
			public int getIndexOfChild(Object parent, Object child)
			{
				if(parent instanceof Container)
					return Arrays.asList(((Container)parent).getComponents()).indexOf(child);
				else
					return -1;
			}
		});
		d.add(tree);
		d.setVisible(true);		
	}

	public String getName()
	{
		return NbBundle.getMessage(ExpandAll.class, "CTL_ExpandAll");
	}

	@Override
	protected void initialize()
	{
		super.initialize();
		// see org.openide.util.actions.SystemAction.iconResource() Javadoc for more details
		putValue("noIconInMenu", Boolean.TRUE);
	}

	public HelpCtx getHelpCtx()
	{
		return HelpCtx.DEFAULT_HELP;
	}

	@Override
	protected boolean asynchronous()
	{
		return false;
	}
}
